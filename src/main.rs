use std::{collections, env, error, fmt, io, sync};

use actix_web::{
    cookie::SameSite,
    error::{ErrorBadRequest, ErrorConflict, ErrorUnauthorized},
    get,
    http::header::ToStrError,
    http::Cookie,
    post, web, App, Error, HttpMessage, HttpRequest, HttpResponse, HttpServer, Responder,
};

use chrono::{Duration, Utc};
use protobuf::Message;

use jsonwebtoken::{
    decode, encode, Algorithm, DecodingKey, EncodingKey, Header, TokenData, Validation,
};
use serde::{Deserialize, Serialize};

use example_api::y_protobuf::auth::{
    AuthenticatePasswordResult_pb, AuthenticatePassword_pb, AuthenticateResponse_pb,
};

#[get("/")]
async fn hello_world() -> impl Responder {
    "Hello, World!"
}

#[post("/renew")]
async fn renew(data: AppData, req: HttpRequest) -> impl Responder {
    match fetch_nonce(&req) {
        Ok(nonce) => match data.db.nonce.register(nonce) {
            Ok(_) => match fetch_auth_token(&req) {
                Ok(token) => match data.jwt.decode_token(token.as_str()) {
                    Ok(_) => match data.jwt.encode_token() {
                        Ok(new_token) => {
                            let mut message = AuthenticateResponse_pb::new();
                            let roles = message.mut_roles();
                            roles.push("admin".into());
                            roles.push("dev-docs".into());
                            match message.write_to_bytes() {
                                Ok(bytes) => HttpResponse::Ok()
                                    .cookie(
                                        // TODO あと Expires ちゃんとする
                                        Cookie::build(COOKIE_AUTH_TOKEN, new_token)
                                            .domain("shun.dev.getto.systems")
                                            .path("/")
                                            .secure(true)
                                            .http_only(true)
                                            .same_site(SameSite::Strict)
                                            .finish(),
                                    )
                                    .body(base64::encode_config(bytes, base64::STANDARD)),
                                Err(_) => HttpResponse::InternalServerError().finish(),
                            }
                        }
                        Err(_) => HttpResponse::InternalServerError().finish(),
                    },
                    Err(_) => HttpResponse::InternalServerError().finish(),
                },
                Err(err) => HttpResponse::from_error(err.into()),
            },
            Err(err) => HttpResponse::from_error(err.into()),
        },
        Err(err) => HttpResponse::from_error(err.into()),
    }
}
#[post("/authenticate")]
async fn authenticate(data: AppData, req: HttpRequest, body: String) -> impl Responder {
    match fetch_nonce(&req) {
        Ok(nonce) => match data.db.nonce.register(nonce) {
            Ok(_) => match base64::decode_config(body, base64::STANDARD) {
                Ok(bytes) => {
                    match AuthenticatePassword_pb::parse_from_bytes(&bytes) {
                        Ok(message) => {
                            if message.get_login_id() == "admin"
                                && message.get_password() == "password"
                            {
                                match data.jwt.encode_token() {
                                    Ok(new_token) => {
                                        let mut result = AuthenticatePasswordResult_pb::new();
                                        result.set_success(true);

                                        let mut message = AuthenticateResponse_pb::new();
                                        let roles = message.mut_roles();
                                        roles.push("admin".into());
                                        roles.push("dev-docs".into());

                                        result.set_value(message);

                                        match result.write_to_bytes() {
                                            Ok(bytes) => HttpResponse::Ok()
                                                .cookie(
                                                    // TODO あと Expires ちゃんとする
                                                    Cookie::build(COOKIE_AUTH_TOKEN, new_token)
                                                        .domain("shun.dev.getto.systems")
                                                        .path("/")
                                                        .secure(true)
                                                        .http_only(true)
                                                        .same_site(SameSite::Strict)
                                                        .finish(),
                                                )
                                                .body(base64::encode_config(
                                                    bytes,
                                                    base64::STANDARD,
                                                )),
                                            Err(_) => HttpResponse::InternalServerError().finish(),
                                        }
                                    }
                                    Err(_) => HttpResponse::InternalServerError().finish(),
                                }
                            } else {
                                HttpResponse::Unauthorized().finish()
                            }
                        }
                        Err(_) => HttpResponse::InternalServerError().finish(),
                    }
                }
                Err(_) => HttpResponse::InternalServerError().finish(),
            },
            Err(err) => HttpResponse::from_error(err.into()),
        },
        Err(err) => HttpResponse::from_error(err.into()),
    }
}

#[get("/demo")]
async fn d(_req: HttpRequest) -> impl Responder {
    let logger = AlwaysLogger::new();

    // request から nonce と token を取り出す、という infra が居るんだな

    let mut action = MyAction::new(/* MyInfra::new() */);
    action.subscribe(move |state| match state {
        MyState::MyEvent(MyEvent::Success) => logger.info("my state success!"),
        MyState::MyEvent(MyEvent::SomethingWrong) => logger.debug("my state something wrong..."),
    });

    match action.ignite().await {
        MyState::MyEvent(MyEvent::Success) => HttpResponse::Ok().finish(),
        MyState::MyEvent(MyEvent::SomethingWrong) => HttpResponse::InternalServerError().finish(),
    }
}

enum MyState {
    MyEvent(MyEvent),
}

struct MyMaterial {
    my_method: MyMethod,
}

impl MyMaterial {
    fn new(/* infra */) -> Self {
        Self {
            my_method: MyMethod::new(/* infra */),
        }
    }
}

struct Action<S, M> {
    pubsub: PubSub<S>,
    material: M,
}

impl<S, M> Action<S, M> {
    fn with_material(material: M) -> Self {
        Self {
            pubsub: PubSub::new(),
            material,
        }
    }

    fn subscribe(&mut self, handler: impl 'static + FnMut(&S)) {
        self.pubsub.subscribe(handler)
    }
}

type MyAction = Action<MyState, MyMaterial>;

impl MyAction {
    fn new(/* infra */) -> Self {
        Self::with_material(MyMaterial::new(/* infra */))
    }

    async fn ignite(self) -> MyState {
        let mut pubsub = self.pubsub;
        let MyMaterial { my_method } = self.material;

        my_method.call(&mut |event| pubsub.post(MyState::MyEvent(event)))
    }
}

struct AlwaysLogger;

trait MyLogger {
    fn new() -> Self;
    fn debug(&self, message: &str);
    fn info(&self, message: &str);
    fn error(&self, message: &str);
}

impl MyLogger for AlwaysLogger {
    fn new() -> Self {
        Self {}
    }
    fn debug(&self, message: &str) {
        println!("{}", message)
    }
    fn info(&self, message: &str) {
        println!("{}", message)
    }
    fn error(&self, message: &str) {
        println!("{}", message)
    }
}

const COOKIE_AUTH_TOKEN: &'static str = "X-GETTO-EXAMPLE-AUTH-TOKEN";
const HEADER_NONCE: &'static str = "X-GETTO-EXAMPLE-API-NONCE";

fn fetch_auth_token(req: &HttpRequest) -> Result<String, FetchError> {
    req.cookie(COOKIE_AUTH_TOKEN)
        .map(|cookie| cookie.value().to_string())
        .ok_or(FetchError::NoAuthToken)
}
fn fetch_nonce(req: &HttpRequest) -> Result<String, FetchError> {
    req.headers()
        .get(HEADER_NONCE)
        .ok_or(FetchError::NoNonceHeader)
        .and_then(|header| {
            header
                .to_str()
                .map(String::from)
                .map_err(FetchError::InvalidNonce)
        })
}

enum MyEvent {
    Success,
    SomethingWrong,
}

struct MyMethod {}

impl MyMethod {
    fn new() -> Self {
        Self {}
    }

    fn call<S>(self, post: &mut impl FnMut(MyEvent) -> S) -> S {
        post(MyEvent::SomethingWrong);
        post(MyEvent::Success)
    }
}

enum AuthEvent {
    Success,
    Error(AuthError),
}

enum AuthError {
    NoToken,
    InvalidToken,
    NoNonceHeader,
    InvalidNonce(ToStrError),
    DuplicateNonce,
    RepositoryError(RepositoryError),
    ValidateError(ValidateError),
}

struct AuthCredential {
    nonce: String,
    token: String,
}

trait AuthRequest {
    fn credential(&self) -> Result<AuthCredential, AuthError>;
}

trait AuthClock {
    fn now(&self) -> ExpiresDateTime;
}

trait AuthNonceRepository {
    fn get(&self, nonce: &str) -> Result<Option<AuthNonce>, RepositoryError>;
    fn put(&self, nonce: AuthNonce) -> Result<(), RepositoryError>;
}

trait AuthTokenValidator {
    fn validate(&self, token: String) -> Result<(), ValidateError>;
}

struct AuthNonce {
    nonce: String,
    registered_at: ExpiresDateTime,
}

impl AuthNonce {
    fn new(nonce: String, clock: &impl AuthClock) -> Self {
        Self {
            nonce,
            registered_at: clock.now(),
        }
    }
}

enum RepositoryError {
    UnknownError,
}

enum ValidateError {
    UnknownError,
}

struct ExpiresDateTime(i64);

impl ExpiresDateTime {
    fn has_elapsed(&self, duration: &ExpiresDuration) -> bool {
        unimplemented!("impl me!")
    }
}

struct ValidateAuthCredentialConfig {
    nonce_expires: ExpiresDuration,
}

struct ExpiresDuration(i64);

trait ValidateAuthCredentialMethod {
    type Request: AuthRequest;
    type Clock: AuthClock;
    type NonceRepository: AuthNonceRepository;
    type TokenValidator: AuthTokenValidator;

    fn request(&self) -> &Self::Request;
    fn clock(&self) -> &Self::Clock;
    fn nonce(&self) -> &Self::NonceRepository;
    fn token(&self) -> &Self::TokenValidator;
    fn config(&self) -> &ValidateAuthCredentialConfig;

    fn call<S>(&self, post: &mut impl FnMut(AuthEvent) -> S) -> StateResult<S> {
        macro_rules! error {
            () => {
                |err| post(AuthEvent::Error(err))
            };
        }
        let request = self.request();

        let credential = request.credential().map_err(error!())?;

        self.check_nonce(credential.nonce).map_err(error!())?;
        self.check_token(credential.token).map_err(error!())?;

        Ok(post(AuthEvent::Success))
    }
    fn check_nonce(&self, value: String) -> Result<(), AuthError> {
        let error = AuthError::RepositoryError;
        let nonce = self.nonce();
        let clock = self.clock();
        let config = self.config();

        if let Some(entry) = nonce.get(value.as_str()).map_err(error)? {
            if !entry.registered_at.has_elapsed(&config.nonce_expires) {
                return Err(AuthError::DuplicateNonce);
            }
        }

        nonce.put(AuthNonce::new(value, clock)).map_err(error)?;

        Ok(())
    }
    fn check_token(&self, value: String) -> Result<(), AuthError> {
        let token = self.token();

        token.validate(value).map_err(AuthError::ValidateError)
    }
}

type StateResult<S> = Result<S, S>;

fn flatten<S>(result: StateResult<S>) -> S {
    match result {
        Ok(inner) => inner,
        Err(inner) => inner,
    }
}

struct PubSub<E> {
    handlers: Vec<Box<dyn FnMut(&E)>>,
}

impl<E> PubSub<E> {
    fn new() -> Self {
        Self { handlers: vec![] }
    }

    fn subscribe(&mut self, handler: impl 'static + FnMut(&E)) {
        self.handlers.push(Box::new(handler));
    }
    fn post(&mut self, event: E) -> E {
        self.handlers.iter_mut().for_each(|handler| handler(&event));
        event
    }
}

#[derive(Debug)]
enum FetchError {
    NoAuthToken,
    NoNonceHeader,
    InvalidNonce(ToStrError),
}

impl fmt::Display for FetchError {
    fn fmt(&self, f: &mut fmt::Formatter<'_>) -> fmt::Result {
        match self {
            FetchError::NoAuthToken => write!(f, "no auth-token found"),
            FetchError::NoNonceHeader => write!(f, "no nonce found"),
            FetchError::InvalidNonce(err) => write!(f, "invalid nonce: {}", err),
        }
    }
}

impl error::Error for FetchError {}

impl Into<Error> for FetchError {
    fn into(self) -> Error {
        match self {
            FetchError::NoAuthToken => ErrorUnauthorized(self),
            FetchError::NoNonceHeader => ErrorBadRequest(self),
            FetchError::InvalidNonce(_) => ErrorBadRequest(self),
        }
    }
}

#[derive(Debug, Serialize, Deserialize)]
struct Claims {
    sub: String,
    exp: i64,
}

#[actix_web::main]
async fn main() -> io::Result<()> {
    let setting = EnvSetting::load();
    let data = web::Data::new(AppState::new());
    HttpServer::new(move || {
        App::new()
            .app_data(data.clone())
            .service(hello_world)
            .service(
                web::scope("/auth")
                    .service(web::scope("/auth-ticket").service(renew))
                    .service(web::scope("/password").service(authenticate)),
            )
    })
    .bind(format!("0.0.0.0:{}", setting.port))?
    .run()
    .await
}

#[derive(Clone)]
struct EnvSetting {
    port: String,
}

impl EnvSetting {
    fn load() -> EnvSetting {
        EnvSetting {
            port: EnvSetting::load_env("PORT"),
        }
    }

    fn load_env(key: &str) -> String {
        env::var(key).expect(format!("env not specified: {}", key).as_str())
    }
}

type AppData = web::Data<AppState>;

struct AppState {
    jwt: JwtKey,
    db: Db,
}

impl AppState {
    fn new() -> AppState {
        AppState {
            jwt: JwtKey::load(),
            db: Db::new(),
        }
    }
}

struct JwtKey {
    private_ec_pem: String,
    public_ec_pem: String,
}

impl JwtKey {
    fn load() -> JwtKey {
        JwtKey {
            // TODO env からじゃなくて SecretManager から読み込む、はず
            private_ec_pem: EnvSetting::load_env("JWT_PRIVATE_KEY"),
            public_ec_pem: EnvSetting::load_env("JWT_PUBLIC_KEY"),
        }
    }

    fn encode_token(&self) -> jsonwebtoken::errors::Result<String> {
        let claims = Claims {
            sub: "jwt-token".to_string(),
            exp: (Utc::now() + Duration::minutes(30)).timestamp(),
        };
        encode(
            &Header::new(Algorithm::ES384),
            &claims,
            &EncodingKey::from_ec_pem(self.private_ec_pem.as_bytes())
                .expect("failed to parse ec pem"),
        )
    }
    fn decode_token(&self, token: &str) -> jsonwebtoken::errors::Result<TokenData<Claims>> {
        let validation = Validation::new(Algorithm::ES384);
        decode::<Claims>(
            token,
            &DecodingKey::from_ec_pem(self.public_ec_pem.as_bytes())
                .expect("failed to parse ec pem"),
            &validation,
        )
    }
}

struct Db {
    nonce: NonceDb,
}

impl Db {
    fn new() -> Db {
        Db {
            nonce: NonceDb::new(),
        }
    }
}

struct NonceDb {
    store: sync::Mutex<collections::HashSet<String>>,
}

impl NonceDb {
    fn new() -> NonceDb {
        NonceDb {
            store: sync::Mutex::new(collections::HashSet::new()),
        }
    }

    fn register(&self, nonce: String) -> Result<(), NonceError> {
        let mut store = self.store.lock().unwrap();
        if store.insert(nonce) {
            Ok(())
        } else {
            Err(NonceError::Duplicate)
        }
    }
}

#[derive(Debug)]
enum NonceError {
    Duplicate,
}

impl fmt::Display for NonceError {
    fn fmt(&self, f: &mut fmt::Formatter<'_>) -> fmt::Result {
        match self {
            NonceError::Duplicate => write!(f, "duplicate nonce"),
        }
    }
}

impl error::Error for NonceError {}

impl Into<Error> for NonceError {
    fn into(self) -> Error {
        match self {
            NonceError::Duplicate => ErrorConflict(self),
        }
    }
}
